source ~/.git-completion.bash
source ~/.git-prompt.sh
source ~/.git-ps1.sh
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced
alias grep="grep --color"
